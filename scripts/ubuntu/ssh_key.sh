#!/bin/bash

set -e
set -x

mkdir ~/.ssh

tee ~/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6f+n2P/DQ9gvsWS4ashoS3pqDBu5wRmlPMItBJBomPEqOFPFE2VbdOPYjSoBiEEO+zJ6O3KDzlo8TZ1gfYsj2Rq1cpCmC1PJH/uoWX2W42lImzkBijcykK0aaHRJA2qWtWDZgp47WiLm79ekoAdaolNwwAq2SHNEok0IR2TdtSBGFr+202z09fWBwmaCZ5XtNRmJ5wKM+yIN7DcMxcREop8kK6je/5G9O4+6/gQYcXVOfA8jr4Z1/ZT5iE9hZi2edn25JbI0AK+P5NisK3foH7G1FIOAEFtgW2rSSG5KlgGplxAGnAkAou8OhKvXJlB74nenu7deadoS648x8jEML
EOF

chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys